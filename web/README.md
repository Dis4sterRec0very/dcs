# How-To Use

Add a stack to Portainer from this GIT repo. Use the http-URL which is also used to clone the repo

## Variables

Add the following variables to make the config complete

- DOMAIN
- TLD

DOMAIN is only the domain part of your fqdn, like example in example.org and org is the TLD
